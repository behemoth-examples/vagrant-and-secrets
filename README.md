# Example - Vagrant and Secrets

A generic method (relaying on Ruby language) to use secrets in Vagrant.

## Importing

Add to your `Vagrantfile`, in it's top

```ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :
require_relative 'vagrant_credentials.rb'
include Secrets

.
.
.
```

## Using

To use the secret, just push them to the environmnet of the vm, and make sure your script/app knows to read from there.

```
.
.
.

    config.vm.provision "shell", path: "scripts/init.sh", privileged: true, env: {"IS_USERNAME" => Secrets::USERNAME, "IS_PASSWORD" => Secrets::PASSWORD }

.
.
.
```

## Ignoring

Don't forget to ignore the file in `.gitignore`, don't push secrets to git!

## Mention Requirments

As the file is ignored, you can create a template of the file before ignoring it, or remind your users to manually create the file.

For example:
"""
This code requires a secret file `vagrant_credentials.rb` with the following information:

```ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :
#
# This file should be added to '.gitignore' and generated manually
# after clone.

module Secrets
    USERNAME = "<your username>"
    PASSWORD = "<your password>"
end
```
"""

## Source

Original credit for this methods goes to [Just a guy coding](https://justaguycoding.com/blog/posts/vagrant-secrets/), I first noticed this solution in his blog.
