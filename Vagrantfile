# -*- mode: ruby -*-
# vi: set ft=ruby :
require_relative 'vagrant_credentials.rb'
include Secrets

Vagrant.configure("2") do |config|
    # Resize ens5's mtu to '1392'.
    # This is an internal issue due to WireGuard VPN.
    config.vm.provision "shell",
        run: "always",
        inline: "sudo ip link set dev ens6 mtu 1392"

    # Initializing system with custom secrets
    config.vm.provision "shell", path: "scripts/init.sh", privileged: true, env: {"IS_USERNAME" => Secrets::USERNAME, "IS_PASSWORD" => Secrets::PASSWORD }

    # Have to disable 'rsync', as it will try to install packages
    # and fail before the route was corrected.
    config.nfs.verify_installed = false
    config.vm.synced_folder './sync', '/vagrant', type: 'rsync', disabled: true

    # Don't inject pub key
    config.ssh.insert_key = false
    config.ssh.username = "vagrant"

    config.vm.box = "generic/debian12"
    config.vm.hostname = "behemoth.example"
    config.vm.network :private_network,
        :ip => "192.168.33.202",
        :libvirt__domain_name => "behemoth.example"

    config.vm.provider :libvirt do |lv|
        lv.machine_virtual_size = 20
        lv.qemu_use_session = false
        lv.cpus = 2
        lv.driver = "kvm"
        lv.memory = 4000
        lv.nic_model_type = "e1000"
        lv.nested = true
    end
end
